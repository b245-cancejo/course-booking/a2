const express = require("express");
const router = express.Router();
const auth = require('../auth.js')

const courseController = require('../Controllers/courseController');

//[Routes without Params]
//Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

//Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses);

//route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses)

//mini-activity
router.get("/allInactive",auth.verify, courseController.allInactiveCourses)






//[Routes with Params]
//Route for retrieving details of specific course 
//router.get("/:courseId", courseController.courseDetails)

router.put("/update/:courseId", auth.verify, courseController.updateCourse);

//Route for activity
router.put("/archiveCourse/:courseId", auth.verify, courseController.archiveCourses)





module.exports = router;
