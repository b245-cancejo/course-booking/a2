
const express = require("express");
const router = express.Router();
const auth = require('../auth.js')

const userController = require("../Controllers/userController.js")

//[Routes]
//this is responsible for the registration of the user
router.post("/register", userController.userRegistration)

//route for user authentication
router.post("/login", userController.userAuthentication)

//route for activity
router.get("/details", auth.verify, userController.getUser)

//route for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);



module.exports = router;