const Course = require('../Models/coursesSchema.js')
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
//add newCourse
module.exports.addCourse = (request, response) => {
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin === true){

		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});
		//saves the created object to our database
		return newCourse.save()
		//course creation successful
		.then(course => {

			//console.log(course);
			response.send("Course Successfully added");
		})
		//course creation failed
		.catch(error => {
			console.log(error);
			response.send(false)
		})
		
	}
	else{
		return response.send("Hindi ka admin Uy!")
	}
	
}

//get allCourses
module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	console.log(userData)

	if(!userData.isAdmin){
		return response.send("You don't have access to this route")
	}else{
		Course.find({})
		.then(result => response.send(result))
		.catch(erro => response.send(error))
	}
}

//create a controller wherein it will retrieve course that are active
module.exports.allActiveCourses = (request, response) => {

	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

//controller wherein it will retrieve inactive courses
module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if (userData.isAdmin){
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}else{
		return response.send("You dont have access on this route")
	}

}

//This controller will get the details of specific courses.

module.exports.courseDetails = (request, response) => {
	//to get the params from url
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

//This controller is for updating specific coures

/*
	Business logic:
		1. We are going to edit/update the course, that is stored in the params
*/

module.exports.updateCourse = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body;


	if(!userData.isAdmin){
		return response.send("You don't have acces in this page!")
	}else{
	
	await Course.findOne({_id: courseId})
		.then(result =>{
			if(result === null){
				return response.send("courseId is invalid, please try again!")
			}else{
				let updatedCourse= {
					name: input.name,
					description: input.description,
					price: input.price
				}
				
				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					console.log(result);
					return response.send(result)})
				.catch(error => response.send(error));
			}
		})
		
	}
}


//activity
module.exports.archiveCourses = (request, response) => {
	const courseId = request.params.courseId;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Course.findByIdAndUpdate(courseId, {isActive: false}, {new: true})
		.then(result => response.send("Successfully Archive Course!"))
		.catch(error => response.send(error))
	}else{
		return response.send("You don't have access on this site!")
	}
}

module.exports.enrollCourse = async (request, response) => {
	// First we have to get the userId and the courseId

	//decode the token to extract/unpack the payload
	const userData = auth.decode(request.headers.authorization);

	//get the courseId by targetting the params in the url
	const courseId = request.params.courseId;

	//2 things that we need to do in this controller
		//First, to push the courseId in the enrollments property of the user
		//second, to push the userId in the enrolles property of the course.

	let isUserUpdated = await User.findById(userData._id)
	.then(result => {
		if(result === null){
			return false
		}
		else{
			result.enrollments.push({courseId: courseId});

			return result.save()
			.then(save => true)
			.catch(error => false)
		}
	})

	let isCourseUpdated = await Course.findById(courseId).then(result => {
		console.log(result);
		if(result === null){
			return false;
		}else{
			result.enrollees.push({userId: userData._id});

			return result.save()
			.then(save => true)
			.catch(error => false);
		}
	})

	console.log(isCourseUpdated);
	console.log(isUserUpdated);

	if(isCourseUpdated && isUserUpdated){
		return response.send("The course is now enrolled!");
	}else{
		return response.send("There was an error during the enrollment. Please try again!");
	}

};
