
const User = require("../Models/usersSchema.js")
const Course = require("../Models/coursesSchema.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


//Controllers

//This controller will create or register a user on our database
module.exports.userRegistration = (request, response) => {
	const input = request.body

	User.findOne({email: input.email})
	.then(result => {
		if (result !== null){
			return response.send("The email is already taken!")
		}else{
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			newUser.save()
			.then(save => {
				return response.send("You are now registered to our website!")
			})
			.catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => console.log(error))
}
module.exports.userAuthentication = (request, response) => {
	let input = request.body;
	//possible scenarios in logging in
		//1. email is not yet registered
		//2. email is registerd but the password is incorrect
	User.findOne({email: input.email})
	.then(result => {
		if (result === null){
			return response.send("Email is not yet registered, Register instead!")
		}else{
			//we have to verify if the password is correct
			//the "compareSync" method is used to compare a non encrypted password to encrypted password.
			//it returns boolean value, if match true value will return otherwise false
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)})
				}else{
					return response.send("Password is incorrect!")
				}
		}
	})
	.catch(error => {
		return response.send(error)
	})
}

//activity

module.exports.getUser = (request, response) => {
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization)

	//console.log(userData);


	// User.findOne({"_id": userData._id})
	User.findById(userData._id)
	.then(result => {
		//avoid to expose sensitive information such as password.
		result.password = "confidential";
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}


module.exports.enrollCourse = async (request, response) => {
	// First we have to get the userId and the courseId

	//decode the token to extract/unpack the payload
	const userData = auth.decode(request.headers.authorization);

	//get the courseId by targetting the params in the url
	const courseId = request.params.courseId;

	//2 things that we need to do in this controller
		//First, to push the courseId in the enrollments property of the user
		//second, to push the userId in the enrolles property of the course.
	let userExist = await User.findById(userData._id)
					.then(result => {
						if(userData.isAdmin){
							return false
						}else if(result === null){
							return false
						}else{
							return true	
						}
					})

	let isUserUpdated = await Course.findById(courseId)
		.then(result => {
			if(result === null){
				return false;
			}else if(userExist){
				result.enrollees.push({userId: userData._id});
				return result.save()
				.then(save => true)
				.catch(error => false)

			}else{
				return false
			}	
		})

	let courseExist = await Course.findById(courseId)
					.then(result => {
						if(result === null){
							return false
						}else{
							return true	
						}
					})
	let isCourseUpdated = await User.findById(userData._id)
				.then(result => {
					if(userData.isAdmin){
						return false
					}
					else if(courseExist){
						result.enrollments.push({courseId: courseId})
						return result.save()
						.then(save => true)
						.catch(error => false)
					}else{
						return false
					}
				})
		console.log(isUserUpdated)
		console.log(isCourseUpdated)

		if(isCourseUpdated && isUserUpdated){
			return response.send("The course is now enrolled!");
		}else{
			return response.send("There was an error during the enrollment. Either your an Admin, Not yet Registered or LogIn First!. Please try again!");
		}

	}
